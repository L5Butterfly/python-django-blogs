#!/usr/bin/python
# -*-encoding=utf8 -*-

from django.contrib import admin

# Register your models here.

from .models import Article

# 注册到admin
admin.site.register(Article)
