#!/usr/bin/python
# -*-encoding=utf8 -*-

from django.urls import path, include

import blog.views

"""http://127.0.0.1:8000/blog/index"""

urlpatterns = [
    path('hello_world', blog.views.hello_world),
    path('content', blog.views.article_content),
    path('index', blog.views.get_index_page),
    path('', blog.views.get_index_page),
    # path('detail', blog.views.get_detail_page),
    path('detail/<int:article_id>', blog.views.get_detail_page)
]